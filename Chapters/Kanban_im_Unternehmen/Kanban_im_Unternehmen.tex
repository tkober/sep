\section{Kanban im Unternehmen}
Nachdem bisher vor Allem die Werte, Prinzipien und Techniken von Kanban vorgestellt wurden, soll dieser Abschnitt Möglichkeiten aufzeigen, wie sich Kanban in Unternehmen integrieren lässt.

\subsection{Ausgangsituation}
In Kanban selbst ist keine Vorgehensweise zur Integration in (bestehende) Unternehmenstrukturen enthalten. 
Daraus resultieren verschiedenste Vorgehensweisen für den Einsatz von Kanban.
Einer der weitverbreitetsten Irrtümer ist, dass Kanban als agiler Ansatz auf Team-Ebene verstanden wird.
Dies ist zwar nicht ausgeschlossen, verschenkt allerdings einen wesentlichen Anteil des Optimierungspotenzials von Kanban, welches eigentlich auf die gesamte Wertschöpfungskette abzielt. 

\subsection{Kanban Flight Levels}
Um eine bessere Orientierung zu geben, wo im Unternehmenskontext Kanban eingesetzt werden kann, entwickelte Klaus Leopold das \emph{Kanban Flight Levels Modell} \cite{Leopold}.
Der folgende Überblick stützt sich maßgeblich auf eine Zusammenfassung der \cite[\emph{LEANability GmbH}]{LEANability}.
Dieses Modell der Flight Levels stellt keines Falls ein Instrument zur Beurteilung dar, wie gut oder schlecht ein Unternehmen Kanban durchführt.
Sie beschreiben ebenso wenig Stufen, die nacheinander umgesetzt werden müssen.
Die Flight Levels sind lediglich ein Informationsinstrument, welches zeigen soll, auf welchen Ebenen Kanban wie einsetzbar ist.


\subsubsection{Flight Levels 1:}
Die untersten beiden Flight Levels beschreiben den Einsatz von Kanban in einzelnen kleinen Organisationseinheiten.

Flight Level 1 beschreibt dabei den Einsatz in Organisationseinheiten ohne regulierten Input.
Das bedeutet, dass der Input unkoordiniert bei der Organisationseinheit ankommt.
Es gibt keine festen Abläufe mit Hilfe derer Stakeholder geordnet Aufgaben einreichen und gegeneinander priorisieren können.
Die Folge ist meist eine stark beanspruchte Organisationseinheit, die mit gleichpriorisierten Aufgaben regelrecht zugeschüttet wird, siehe \autoref{fig:FL1}.

Charakteristisch sind diese Eigenschaften vor allem für hochspezialisierte Teams, wie sie häufig in Hightech-Unternehmen, etwa Luft- und Raumfahrt, gefunden werden.
Eine weitere Art von Organisationseinheiten, die häufig in ähnlichen Verhältnissen arbeiten, stellen hochgradig cross-funktionale Teams dar.

\begin{figure}[H]
	\centering
	\includegraphics[width=.6\textwidth]{Chapters/Kanban_im_Unternehmen/Kanban-Flight-Level-1}
	\caption{Organisationseinheit ohne regulierten Input}
	\label{fig:FL1}
\end{figure}

Durch die hohe Spezialisierung dieser Organisationseinheiten sind meist eine Vielzahl an Stakeholdern an den Services dieser einen Organisationseinheit interessiert.
Zweifellos kann Kanban die Situation hier durch Visualisierung und Limitierung des Workloads erheblich verbessern, allerdings stößt es auf Flight Level eins auch an einige Grenzen.
Der größte Schwachpunkt ist die fehlende Input-Koordination, was dazu führt, dass dies Aufgaben ständig von der Organisationseinheit neu priorisiert werden.
Da die Stakeholder bei dieser Priorisierung nicht mit einbezogen werden, besteht ein hohes Risiko, dass die Organisationseinheit die Abläufe für sich selbst, aber nicht für die gesamte Wertschöpfungskette optimiert.
Dennoch ist der Einsatz von Kanban in solchen Organisationseinheiten ohne regulierten Input zu empfehlen, da es zumindest den Workload reduzieren wird.
Dieser ist gerade in den oben beschriebenen Arten von Organisationseinheiten meist enorm und kann negative Auswirkungen auf Moral und Gesundheit der beteiligten Mitarbeiter haben, was sich wiederum negativ auf deren Arbeitsleistung und somit auf die gesamte Wertschöpfungskette auswirkt.


\subsubsection{Flight Levels 2:}
Flight Level 2 beschreibt ebenfalls den Einsatz von Kanban auf der Ebene einzelner Organisationseinheiten.
Der entscheidende Unterschied zum Flight Level 1 ist hierbei allerdings, dass der Input koordiniert wird.

Eine typische Vorgehensweise ist es, Tasks in einer limitierten Input-Queue zu verwalten und neue Tasks unter Einbeziehung aller Stakeholder in sogenannten \emph{Queue Replenishment Meeting} hinzuzufügen, siehe \autoref{fig:FL2}.
Der entscheidende Vorteil eines solchen Vorgehens ist, dass alle beteiligten lernen, dass eine Organisationseinheit keine Black-Box ist, die im gleichen Maße Ergebnisse zu Tage fördert wie Arbeit hineingeworfen wird.
Stattdessen wird ein Bewusstsein dafür geschaffen, dass es einen Bedarf an Services auf Seiten der Stakeholder gibt und begrenzte Möglichkeiten auf Seiten der entsprechenden Organisationseinheit.
Den Input zu koordinieren bedeutet nun dieses Verhältnis zwischen Bedarf und Möglichkeiten im Gleichgewicht zu halten.
Dazu müssen die Stakeholder ihre Wünsche im Dialog abstimmen und entsprechend priorisieren.
\begin{figure}[H]
	\centering
	\includegraphics[width=.6\textwidth]{Chapters/Kanban_im_Unternehmen/Kanban-Flight-Level-2}
	\caption{Organisationseinheit mit koordiniertem Input}
	\label{fig:FL2}
\end{figure}
Durch einen koordinierten Input wird nicht nur die Organisationseinheit selbst entlastet, da nun ein Pull-Prinzip angewandt werden kann.
Durch die gemeinsame Priorisierung wird auch erreicht, dass bei der Optimierung der Fokus nicht mehr nur auf einer Organisationseinheit liegt.
Die Wahrscheinlichkeit \emph{das richtige} effizienter zu machen ist damit deutlich höher als auf Flight Level 1.
Allerdings handelt es sich hierbei oftmals auch nur um eine lokale Optimierung, da meist nur Teile der gesamten Wertschöpfungskette mit einbezogen werden.
Dadurch wird der eigentliche Mehrwert für den Kunden eventuell nicht in dem Maße gesteigert, wie die Produktivität der Organisationseinheit.


\subsubsection{Flight Levels 3:}
Genau an dieser Stelle setzt der Flight Level 3 an.
Dieser legt das Hauptaugenmerk auf die Optimierung der gesamten Wertschöpfungskette, indem Zusammenarbeit der einzelnen Organisationseinheiten verbessert wird.

\autoref{fig:FL3} illustriert ein Beispiel, in welchem Eine Organisation Kunden den Service anbietet ein Brief zu schreiben.
Jede Taste wird von einer anderen Organisationseinheit gesteuert.
Die Organisationseinheit, welche die Taste \emph{A} betreut, könnte jetzt enormen Aufwand in die Optimierung des Task \emph{Tastendruck von A} stecken.
Der Zuwachs der Performance des gesamten Systems wird allerdings minimal ausfallen, obwohl eventuell enormer Aufwand betrieben wurde.
\begin{figure}[H]
	\centering
	\includegraphics[width=.6\textwidth]{Chapters/Kanban_im_Unternehmen/Kanban-Flight-Levels-Tastatur}
	\caption{Service \emph{Brief schreiben} mit vielen beteiligten Organisationseinheiten}
	\label{fig:FL3}
\end{figure}
Auf Flight Level 3 ist es entscheidend, dass die Organisation versteht, dass die \emph{Performance eines Systems nicht die Summe seiner Einzelteile, sondern das Ergebnis seiner Interaktionen} ist.
Dies soll durch ein sogenanntes \emph{Serviceorientiertes Kanban} erreicht werden.
Dabei stellen Organisationseinheiten (interne) Services für andere Organisationseinheiten bereit.
Dies ist in vielen größeren Unternehmen bereits gängige Praxis.
Der entscheidende Unterschied besteht allerdings darin, dass diese Services koordiniert in Anspruch genommen werden.
Kanban ermöglicht auf diesem Flight Level enorme Performancesteigerungen, da bei korrekter Anwendung die Größe der Input-Queue signifikant reduziert werden kann und die Organisationsarbeiten jetzt entlang der gesamten Wertschöpfungskette an \emph{den richtigen Dingen} arbeiten.

\subsubsection{Flight Levels 4:}
Flight Level 4 setzt auf der höchsten Ebene der Wertschöpfung an, dem (Service-)Portfolio.
Im Allgemeinen schafft ein Unternehmen nicht mit nur einem Produkt oder Service Mehrwert für seine Kunden, sondern mit einem Mix aus verschiedenen, wie in \autoref{fig:FL4} dargestellt.
Daraus ergibt sich die Situation, dass es mehrere Wertschöpfungsketten gibt, von denen die Gesamtperformance abhängt.
\begin{figure}[H]
	\centering
	\includegraphics[width=.6\textwidth]{Chapters/Kanban_im_Unternehmen/Kanban-Flight-Level-4}
	\caption{Unternehmen mit verschiedenen Wertschöpfungsketten}
	\label{fig:FL4}
\end{figure}
Auch hier lässt sich Kanban zur Effizienzsteigerung einsetzen, indem es diese Wertschöpfungsketten koordiniert.
Das bedeutet es wird auch hier ein Gleichgewicht zwischen dem Bedarf des Kunden oder des Marktes und den Möglichkeiten des Unternehmens hergestellt und die verschiedenen Wertschöpfungsketten priorisiert.
\\
\\
Abschließend ist es wichtig, nochmals klarzustellen, dass das Flight Level Modell keine Gütestufen für eine Kanban-Umsetzung im Unternehmen darstellt.
Es zeigt vielmehr auf, dass Kanban nicht nur auf der Ebene einzelner Organisationseinheiten einsetzbar ist, sondern auch auf höheren Ebenen, auf welchen es meist ein wesentlich höheres Potenzial an Effizienzsteigerung mit sich führt.



